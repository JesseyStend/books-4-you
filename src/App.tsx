import * as React from "react";
import { Container } from "@mui/material";
import Header from "./Components/Header";
import Table from "./Components/Table";
import { BrowserRouter, Route, Routes } from "react-router-dom";
import Book from "./Components/Book";
import AddABook from "./Components/AddABook";
import { SnackbarProvider } from "notistack";

export default function App() {
  return (
    <SnackbarProvider>
      <BrowserRouter>
        <Header />
        <main style={{height: "calc(100vh - 300px)", width: "100vw", overflow: "auto"  }}>
          <Routes>
            <Route path='/Add-a-book' element={<AddABook/>} />
            <Route path='/Book/:id' element={<Book/>} />
            <Route path='*' element={<Table />} />
          </Routes>
        </main>
      </BrowserRouter>
    </SnackbarProvider>
  );
}
