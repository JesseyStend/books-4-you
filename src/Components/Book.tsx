import { Class, DateRange } from "@mui/icons-material";
import { Avatar, Button, CardActions, CardContent, CardHeader, CardMedia, Divider, Grid, styled, Typography } from "@mui/material";
import * as React from "react";
import { useState, useEffect } from "react";
import { useParams, useNavigate } from "react-router-dom";
import { IGoogleBooksResponse, ISearchResult } from "./Table";

interface BookProps {}

interface IBook extends ISearchResult {
    subtitle: string;
    thumbnail: string;
    description: string;
    genre?: string;
    link?: string;
}

const InformationHeader = styled(CardHeader)({
    paddingTop: "4px", paddingBottom: "4px"
})

const Book: React.FunctionComponent<BookProps> = () => {
  const [book, setBook] = useState<IBook | undefined>(undefined);
  const navigate = useNavigate();
  const { id } = useParams();

  useEffect(() => {
    fetch(`https://www.googleapis.com/books/v1/volumes/${id}`)
      .then((r) => r.json())
      .then((item: IGoogleBooksResponse) => ({
        id: item.id,
        isbn: item?.volumeInfo?.industryIdentifiers?.map((identifier, index) => `${identifier.identifier} - ${identifier.type}`),
        title: item.volumeInfo.title,
        subtitle: item.volumeInfo.subtitle,
        link: item.saleInfo.buyLink,
        description: item.volumeInfo.description,
        authors: item.volumeInfo.authors,
        publishedDate: item.volumeInfo.publishedDate,
        thumbnail: item.volumeInfo.imageLinks.thumbnail,
        genre: item?.volumeInfo?.categories?.[0]
      })).then(setBook);
  }, [id]);

  return (
    <div style={{ width: "100%", height: "100%" }}>
      <Grid container>
        <Grid item xs={2} sx={{ p: "16px" }}>
          <CardMedia component='img' image={book?.thumbnail} alt='Book tumbnail' />
        </Grid>
        <Grid item xs='auto'>
          <Divider orientation='vertical' variant='middle' />
        </Grid>
        <Grid item xs>
          <CardHeader sx={{paddingBottom: "32px"}} title={`${book?.title} ${book?.subtitle? `- ${book.subtitle}` : ""}`} subheader={`By ${book?.authors.join(", ")}`} action={<Button onClick={()=> navigate(-1)}>Go back</Button>}/>
          <Grid container spacing={1}>
            {book?.genre && (
              <Grid item xs="auto">
                  <InformationHeader avatar={<Avatar><Class/></Avatar>} title="genre" subheader={book?.genre}/>
              </Grid>
            )}
            <Grid item xs="auto">
                <InformationHeader avatar={<Avatar><DateRange/></Avatar>} title="Date of publishing" subheader={book?.publishedDate}/>
            </Grid>
          </Grid>
          {book?.description && <div style={{padding: "16px 16px 0px 16px"}} dangerouslySetInnerHTML={{__html: book.description}}/>}
        </Grid>
      </Grid>
      {book?.link && (
        <CardActions disableSpacing>
          <Button sx={{marginLeft: "auto"}} href={book.link}>Go to shop</Button>
        </CardActions>
      )}
    </div>
  );
};

export default Book;
