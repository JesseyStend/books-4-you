import * as React from "react";
import { useState, useEffect } from "react";
import { TableHead, Table, TableBody, TableRow, TableCell, styled, useTheme, Typography, Paper, CircularProgress, Fade } from "@mui/material";
import { useSearchParams, useNavigate } from "react-router-dom";
import { ArrowDownward, ArrowUpward } from "@mui/icons-material";

export interface TableProps {}
export interface ISearchResult {
  id: number;
  isbn: string[] | undefined;
  title: string;
  authors: string[];
  publishedDate: string;
}

export interface IGoogleBooksResponse {
  id: number;
  volumeInfo: {
    title: string;
    subtitle: string;
    description: string;
    categories: string[];
    authors: string[];
    publishedDate: string;
    industryIdentifiers: {
      identifier: string;
      type: string;
    }[];
    imageLinks: {
      large: string;
      small: string;
      thumbnail: string;
    };
  };
  saleInfo: {
    buyLink: string;
  };
}

const ClickableRow = styled(TableRow, { shouldForwardProp: (prop: any) => prop })(({ theme }) => ({
  background: "rgb(21,21,21,0)",
  transition: theme.transitions.create("background", {
    easing: theme.transitions.easing.easeOut,
    duration: theme.transitions.duration.leavingScreen,
  }),
  "&:hover": {
    transition: theme.transitions.create("background", {
      easing: theme.transitions.easing.easeIn,
      duration: theme.transitions.duration.enteringScreen,
    }),
    background: "rgb(21,21,21,0.10)",
    cursor: "pointer",
  },
}));

const ClickableTableCell = styled(TableCell)(({ theme }) => ({
  background: "rgb(21,21,21,0)",
  transition: theme.transitions.create("background", {
    easing: theme.transitions.easing.easeOut,
    duration: theme.transitions.duration.leavingScreen,
  }),
  "&:hover": {
    transition: theme.transitions.create("background", {
      easing: theme.transitions.easing.easeIn,
      duration: theme.transitions.duration.enteringScreen,
    }),
    background: "rgb(21,21,21,0.10)",
    cursor: "pointer",
  },
}));
interface IToSort {
  [key: string]: any;
}

const TableComponent: React.FunctionComponent<TableProps> = () => {
  const [searchResults, setSearchResults] = useState<ISearchResult[]>([]);
  const [isLoading, setIsLoading] = useState(false);
  const theme = useTheme();
  const [searchParam] = useSearchParams();
  const searchTerm = searchParam.get("searchTerm");
  const navigate = useNavigate();
  const [toSort, setToSort] = useState(["id", "asc"]);
  const [sortKey, direction] = toSort;

  useEffect(() => {
    if (searchTerm) {
      setIsLoading(true);
      if (searchTerm) {
        fetch(`https://www.googleapis.com/books/v1/volumes?q=${searchTerm}`)
          .then((r) => r.json())
          .then((r) => {
            let parsedResult = r.items.map((item: IGoogleBooksResponse) => ({
              id: item.id,
              isbn: item?.volumeInfo?.industryIdentifiers?.map((identifier, index) => `${identifier.identifier} - ${identifier.type}`),
              title: item.volumeInfo.title,
              authors: item.volumeInfo.authors,
              publishedDate: item.volumeInfo.publishedDate,
            }));
            setSearchResults(parsedResult);
            setTimeout(() => {
              setIsLoading(false);
            }, 1250);
          })
          .catch(() => setIsLoading(false));
      }
    }
    return () => {
      setSearchResults([]);
    };
  }, [searchTerm]);

  return (
    <div style={{ position: "relative" }}>
      <Fade in={isLoading}>
        <div style={{zIndex: 100, position: "fixed", height: "100%", width: "100%", background: "linear-gradient(to top, rgba(0,0,0,0) 0%, rgba(1,1,1,0.6) 100%)" }}>
          <Paper sx={{ position: "absolute", top: "25%", left: "50%", transform: "translate(-50%, -50%)", pt: "5px" }}>
            <iframe src='https://giphy.com/embed/2uI6CmpEWzpZI8BXvf' width='250' frameBorder='0' className='giphy-embed' allowFullScreen></iframe>
            <Typography fontWeight='600' textAlign='center'>
              {searchResults?.length > 0 ? "Found some!" : "Searching..."}
            </Typography>
          </Paper>
        </div>
      </Fade>
      {searchResults?.length > 0 ? (
        <Table stickyHeader>
          <TableHead>
            <TableRow
              sx={{
                [theme.breakpoints.down("md")]: {
                  "& > th": {
                    fontSize: "13px",
                  },
                },
              }}>
              {Object.keys(searchResults[0]).map((key) => (
                <ClickableTableCell onClick={() => setToSort([key, key === sortKey && direction === "asc" ? "desc" : "asc"])}>
                  <Typography noWrap sx={{ display: "flex", alignItems: "center", fontWeight: 500, fontSize: "13px" }}>
                    {key}
                    {sortKey === key && (direction === "asc" ? <ArrowUpward sx={{ p: "5px" }} /> : <ArrowDownward sx={{ p: "5px" }} />)}
                  </Typography>
                </ClickableTableCell>
              ))}
            </TableRow>
          </TableHead>

          <TableBody>
            {searchResults
              .sort((a: IToSort, b: IToSort) => {
                if (a[sortKey] > b[sortKey]) return direction === "asc" ? -1 : 1;
                if (a[sortKey] < b[sortKey]) return direction === "asc" ? 1 : -1;
                return 0;
              })
              .map((result: ISearchResult) => {
                return (
                  <ClickableRow onClick={() => navigate(`book/${result.id}`)}>
                    {Object.entries(result).map(([key, value]) => (
                      <TableCell>
                        <Typography noWrap maxWidth={"20vw"}>
                          {value ? (value.constructor === Array ? value.join(", ") : value) : "No data"}
                        </Typography>
                      </TableCell>
                    ))}
                  </ClickableRow>
                );
              })}
          </TableBody>
        </Table>
      ) : (
        <Typography sx={{ p: "2px" }} variant='h5' textAlign='center'>
          No data found
        </Typography>
      )}
    </div>
  );
};

export default TableComponent;
