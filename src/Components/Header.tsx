import { Add, Search } from "@mui/icons-material";
import { Button, FormHelperText, Grid, InputAdornment, Paper, styled, TextField, TextFieldProps, Typography } from "@mui/material";
import { useTheme } from "@mui/system";
import * as React from "react";
import { useSearchParams, useNavigate } from "react-router-dom";

interface HeaderProps {}

const SearchField = styled(
  (
    props: TextFieldProps & {
      border?: string;
    }
  ) => <TextField {...props} />
)(({ theme, ...props }) => ({
  "& > label": {
    transform: "translate(14px, 8px) scale(1)",
  },
  "& > .MuiInputLabel-shrink": {
    transform: "translate(14px, -10px) scale(0.8)",
  },
  "& > .MuiOutlinedInput-root": {
    "& > .MuiOutlinedInput-input": {
      padding: props.select ? `10px 35px 10px 14px` : props.multiline ? "" : `10px 14px`,
    },
    "& > .MuiOutlinedInput-notchedOutline": {
      border: !props.error ? "unset" : props.border,
    },
    borderRadius: "20px",
    background: "white",
  },

  [theme.breakpoints.down("md")]: {
    width: "400px",
  },
  [theme.breakpoints.up("md")]: {
    width: "600px",
  },
  [theme.breakpoints.down("sm")]: {
    width: "300px",
  },
}));

const Header: React.FunctionComponent<HeaderProps> = () => {
  const theme = useTheme();
  let [searchParams] = useSearchParams();
  const navigate = useNavigate();

  /**
   * This function is used to fetch books with the given searchterm.
   * @param event is a FormEven that is used to get the value of the searchterm.
   */
  const handleSearch: React.FormEventHandler<HTMLFormElement> = (event) => {
    let form = event.currentTarget;
    let formData = new FormData(form);
    let searchTerm = formData.get("searchterm") as string;
    navigate(`/?searchTerm=${searchTerm}`);
    event.preventDefault();
  };

  return (
    <header>
      <Paper
        elevation={0}
        square
        sx={{
          background: theme.palette.primary.main,
          minHeight: "300px",
          borderBottom: `5px solid ${theme.palette.primary.light}`,
          display: "flex",
          justifyContent: "center",
          alignItems: "center",
        }}>
        <form onSubmit={handleSearch}>
          <Typography variant='h3' textAlign='center' color='white' m={"20px"}>
            Books 4 you!
          </Typography>
          <SearchField
            defaultValue={searchParams.get("searchTerm")}
            name='searchterm'
            InputProps={{
              startAdornment: (
                <InputAdornment position='start'>
                  <Search />
                </InputAdornment>
              ),
            }}
          />
          <FormHelperText sx={{ color: "white", ml: "20px" }}>Press enter to search</FormHelperText>
          <div
            style={{
              display: "flex",
              justifyContent: "center",
              alignItems: "center",
            }}>
            <Button href="Add-a-book" startIcon={<Add />} color='secondary' sx={{ m: "10px auto" }} variant='contained'>
              Add book
            </Button>
          </div>
        </form>
      </Paper>
    </header>
  );
};

export default Header;
function useQueryParam<T>(arg0: string): [any, any] {
  throw new Error("Function not implemented.");
}
