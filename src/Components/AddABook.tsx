import { Class, DateRange } from "@mui/icons-material";
import { Avatar, Button, CardActions, CardContent, CardHeader, CardMedia, Divider, Grid, InputLabel, MenuItem, styled, Typography } from "@mui/material";
import { useSnackbar } from "notistack";
import * as React from "react";
import { useState, useEffect } from "react";
import { useParams, useNavigate } from "react-router-dom";
import clickToUpload from "../Assets/ClickToUpload.png"
import TextField from "./TextField";
const ISBN = require( 'isbn-validate' );

interface BookProps {}

const Input = styled('input')({
  display: 'none',
});


const InformationHeader = styled(CardHeader)({
  paddingTop: "4px",
  paddingBottom: "4px",
});

const AddABook: React.FunctionComponent<BookProps> = () => {
  const [previewOfThumbnail, setPreviewOfThumbnail] = useState<string | undefined>()
  const [isbn, setIsbn] = useState<string>()  
  const navigate = useNavigate();
  const { enqueueSnackbar } = useSnackbar();

  const handleThumbnailChange:  React.ChangeEventHandler<HTMLInputElement> = (e)=>{
    if (!e.target.files || !e.target.files[0]) return;
    const file = e.target.files[0];
    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload= (readerEvent) => setPreviewOfThumbnail(readerEvent!.target!.result! as string)
  }

  const handleSubmit: React.FormEventHandler<HTMLFormElement> = (event) => {
    let form = event.currentTarget;
    let formData = new FormData(form);
    if (isbn && ISBN.Validate(isbn))
      fetch(`https://www.googleapis.com/books/v1/volumes/add`, {
        headers: {
          "Content-Type": "multipart/form-data",
        },
        method: 'POST',
        body: formData,
      }).then(()=>{
        enqueueSnackbar("Book successfully added", {variant: "success"})
        navigate("/")
      }).catch(()=>{
        enqueueSnackbar("Book successfully added", {variant: "success"})
        navigate("/")
      })
    else {
      enqueueSnackbar("Isbn needs to be a valid 10-digit or 13 digit isbn", {variant: "error"})
    }
    event.preventDefault();
  };

  return (
    <form style={{ width: "100%", height: "100%" }} onSubmit={handleSubmit}>
      <Grid container>
        <Grid item xs={3} sx={{ p: "16px" }}>
          <label htmlFor='upload-thumbnail'>
            <Input onChange={handleThumbnailChange} accept='image/*' id='upload-thumbnail' name='upload-thumbnail' type='file' />
            <CardMedia component='img' image={previewOfThumbnail || clickToUpload} alt='Book tumbnail' />
          </label>
        </Grid>
        <Grid item xs='auto'>
          <Divider orientation='vertical' variant='middle' />
        </Grid>
        <Grid item xs>
          <CardContent>
            <Grid container spacing={1}>
              <Grid item xs={12}>
                <Typography sx={{p: "16px"}} variant="h5" color="initial">Volume information</Typography>
              </Grid>
              <Grid item xs={4}>
                <TextField required fullWidth label="Title of the book" name="title"/>
              </Grid>
              <Grid item xs={8}>
                <TextField fullWidth label="Subtitle" name="subtitle"/>
              </Grid>
              <Grid item xs="auto">
                <TextField InputLabelProps={{ shrink: true }} required fullWidth type="date" label="Date of publishing" name="publishedDate"/>
              </Grid>
              <Grid item xs>
                <TextField required fullWidth label="Genre" name="categories"/>
              </Grid>
              <Grid item xs>
                <TextField required fullWidth label="Author(s)" name="authors"/>
              </Grid>
              <Grid item xs={12}>
                <TextField required fullWidth multiline minRows={3} label="Description" name="description"/>
              </Grid>
              <Grid item xs={12}>
                <Typography sx={{p: "16px"}} variant="h5" color="initial">Industry identifier</Typography>
              </Grid>
              <Grid item xs>
                <TextField onChange={({target: {value}}) => setIsbn(value)} fullWidth label="isbn" error={Boolean(isbn && !ISBN.Validate(isbn || ""))}/>
              </Grid>
            </Grid>
          </CardContent>
          <CardActions>
            <Button type="submit">Submit</Button>
          </CardActions>
        </Grid>
      </Grid>
    </form>
  );
};

export default AddABook;
