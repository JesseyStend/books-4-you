import * as React from 'react';
import { styled, TextField as MuiTextField, TextFieldProps } from "@mui/material";

const TextField = styled(
  (
    props: TextFieldProps
  ) => <MuiTextField {...props} />
)(({ theme, ...props }) => ({
  "& > label": {
    transform: "translate(14px, 8px) scale(1)",
  },
  "& > .MuiInputLabel-shrink": {
    transform: "translate(14px, -10px) scale(0.8)",
  },
  "& > .MuiOutlinedInput-root": {
    "& > .MuiOutlinedInput-input": {
      padding: props.select ? `10px 35px 10px 14px` : props.multiline ? "" : `10px 14px`,
    },
    borderRadius: "20px",
    background: "white",
  },
}));

export default TextField;